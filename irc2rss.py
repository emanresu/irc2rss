from feedgen.feed import FeedGenerator
import threading
import socket
import time
import sys
import re

HOST = "irc.rizon.net"
PORT = 7000
IRC_CHANNEL = "#deadinternettheory"
IRC_NICK = "deadbot"
IRC_PASSWORD = ""
FEED_URL = "http://yourdomain.com/rss/rss.xml"
FEED_ID = "https://yourdomain.com"
RSS_PATH = "server/www/rss/rss.xml"

IRC_COMMANDS = ["NICK " + IRC_NICK + "\r\n",
"USER " + IRC_NICK + " " + IRC_NICK + "_ " + IRC_NICK + "__ " + IRC_NICK + "___ " + ":" + IRC_NICK + "\r\n",
"JOIN " + IRC_CHANNEL + "\r\n",
"NICKSERV IDENTIFY " + IRC_PASSWORD + "\r\n"]

irc_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
fg = FeedGenerator()

def generateNewFeed():
    fg.id(FEED_ID)
    fg.title(HOST + " " + IRC_CHANNEL)
    fg.subtitle('IRC to RSS feed bot.')
    fg.link(href=FEED_URL, rel='self')
    fg.language('en')
    fg.rss_file(RSS_PATH)


def listenToIRC():
    while True:
        #get response from irc server
        ircData = irc_socket.recv(2040).decode('utf-8')
        #respond to ping requests
        if ircData[0:4] == "PING":
            irc_socket.send(("PONG" + ircData[4:] + "\r\n").encode())
        #send messages to whevever
        if "PRIVMSG" in ircData and IRC_CHANNEL in ircData and not "PING" in ircData:
            print(ircData)
            fe = fg.add_entry()
            fe.id(FEED_ID)
            fe.title(HOST + " " + IRC_CHANNEL)
            exUsr = ircData[20:]
            exData = re.sub('^(.*PRIVMSG)', "", ircData)
            data = exUsr + " " + exData
            fe.description(data.encode('ascii', errors='ignore'))
            fe.link(href=FEED_URL)
            fg.rss_file(RSS_PATH)
            print(data)

def main():
    try:
        generateNewFeed()

        #start IRC listening thread
        irc_socket.connect((HOST, PORT))
        irc_thread = threading.Thread(target=listenToIRC, args=())
        irc_thread.start()

        #send NICK
        time.sleep(5)
        irc_socket.send(IRC_COMMANDS[0].encode())

        #send USER
        time.sleep(1)
        irc_socket.send(IRC_COMMANDS[1].encode())

        #send login credentials
        if IRC_PASSWORD:
            time.sleep(2)
            irc_socket.send(IRC_COMMANDS[3].encode())

        #send JOIN
        time.sleep(1)
        irc_socket.send(IRC_COMMANDS[2].encode())

        #keep the main thread going
        while True:
            time.sleep(1)

    except KeyboardInterrupt as e:
        irc_thread.join()
        irc_socket.close()
        sys.exit(str(e))

if __name__ == "__main__":
    main()
